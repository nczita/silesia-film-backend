from flask import Blueprint, jsonify, request
import logging


logger = logging.getLogger(__name__)
blueprint = Blueprint("pact_api", __name__)

@blueprint.route("/provider-states", methods=["GET"])
def get_provider_states():
    return jsonify({
            "silesia-film-frontend" : [
                "provider have comments for film",
                "provider have film",
                "provider allows film creation"]
        })

@blueprint.route("/provider-states", methods=["POST"])
def set_provider_state():
    request_json = request.get_json()
    consumer = request_json.get("consumer")
    state = request_json.get("state")
    logger.info(f"Consumer: {consumer} set provider state request: {state}")
    if consumer == "silesia-film-frontend":
        logger.info(f"Consumer: {consumer} setting state '{state}'")
        if state == 'provider have film':
            logger.info("here we should add database entry with film of requested id")
        elif state == 'provider allows film creation':
            logger.info("no operation needed")
        elif state == 'provider have comments for film':
            logger.info("here we should add some comments for film of requested id")
        else:
            logger.warn(f"Consumer: {consumer} have no state '{state}'")
    return jsonify()

@blueprint.route("/provider-states", methods=["DELETE"])
def delete_provider_state():
    request_json = request.get_json()
    consumer = request_json.get("consumer")
    state = request_json.get("state")
    logger.info(f"Consumer: {consumer} set provider state request: {state}")
    if consumer == "silesia-film-frontend":
        logger.info(f"Consumer: {consumer} cleanunp state '{state}'")
        if state == 'provider have film':
            logger.info("here we should clean database")
        elif state == 'provider allows film creation':
            logger.info("no operation needed")
        elif state == 'provider have comments for film':
            logger.info("here we should clean database")
        else:
            logger.warn(f"Consumer: {consumer} have no state '{state}'")
    return jsonify()
