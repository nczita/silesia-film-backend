#!/bin/bash

FLASK_APP=service_pact.py FLASK_ENV=development flask run --host=0.0.0.0 --port 8000 &
FLASK_JOB_PID=$!

PACT_URL=${PACT_BROKER_URL}/pacts/provider/silesia-film-backend/consumer/silesia-film-frontend/latest
PROVIDER_BASE_URL=http://localhost:8000
PROVIDER_STATES_URL=http://localhost:8000/pact/provider-states
PROVIDER_STATES_ACTIVE_URL=http://localhost:8000/pact/provider-states/active
GIT_SHA=$(git rev-parse --verify HEAD)

pact-provider-verifier $PACT_URL \
    -r --provider-app-version=$GIT_SHA \
    --provider-base-url=$PROVIDER_BASE_URL \
    --provider-states-setup-url=$PROVIDER_STATES_URL

kill $FLASK_JOB_PID
