FROM python:3.7-slim

WORKDIR /usr/src/app

COPY requirements.txt .

RUN pip3 install --no-cache -r requirements.txt

COPY . /usr/src/app

ENV GUNICORN_CMD_ARGS "--bind=0.0.0.0:8000"
EXPOSE 8000

CMD ["gunicorn", "service_pact:app", "${GUNICORN_CMD_ARGS}"]