# SilesiaFilm

SilesiaFilm application shows shows statistics and comments of various film productions.

This is a backend application of SilesiaFilm system.

## Milestone 1

In this milestone our app should allow user to:
- add new film
- get basic info about specific film
- get comments about specific film

## Milestone 2

In this milestone our app should allow user to:
- get list of added films
- get rating of films
- extend description of film object with a film poster

## Installation

This project was created with Flask.


I have added `flask` and `gunicorn` dependencies:

```
$ pip install -U flask
$ pip install -U flask
```

To install `pact-provider-verifier` locally we need recent version of Ruby.

Add install there this tool:
```
$ gem install pact-provider-verifier
```

## Acknowledges

[Pact documentation](https://docs.pact.io/getting_started/verifying_pacts)

[Flask documentation](http://flask.pocoo.org/)

[Pact Provider Verifier](https://github.com/pact-foundation/pact-provider-verifier)
