import logging

from flask import Flask, jsonify

logger = logging.getLogger(__name__)

APP_NAME = "silesia_film"


def create_app(config):
    app = Flask(APP_NAME)
    app.config.from_object(config)

    from .api import film_api, comments_api

    app.register_blueprint(film_api.blueprint)
    app.register_blueprint(comments_api.blueprint)

    return app
