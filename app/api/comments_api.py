from flask import Blueprint, jsonify


blueprint = Blueprint("comments", __name__)


@blueprint.route("/comments/<commented_item_id>", methods=["GET"])
def get_comments_for_item(commented_item_id):
    return jsonify([{
        "id": "8e8579a7-a72a-4048-9894-ae7fdd471a55",
        "commentedId": commented_item_id,
        "author": "Some Guy",
        "text": "Need to watch it!"
    }])
