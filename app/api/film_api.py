from flask import Blueprint, jsonify
import uuid

blueprint = Blueprint("film", __name__)


@blueprint.route("/films/<film_id>", methods=["GET"])
def get_film_by_id(film_id):
    return jsonify({
        "id": film_id,
        "title": "Superman Returns",
        "year": "2006",
        "rating": 6.1
    })


@blueprint.route("/films", methods=["POST"])
def create_a_film():
    return jsonify({
        "id": uuid.uuid4(),
        "title": "Superman Returns",
        "year": "2006",
        "rating": 6.1
    }), 201
