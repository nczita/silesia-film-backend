import os
import logging

from app import create_app

app = create_app(os.environ)

from tests.pact import pact_api

app.register_blueprint(pact_api.blueprint, url_prefix='/pact')
